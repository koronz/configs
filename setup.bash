#!/bin/bash

# Linking config files
ln -sf $PWD/.bashrc $HOME/.bashrc && echo ".bashrc linked"
ln -sf $PWD/.vimrc $HOME/.vimrc && echo ".vimrc linked"
ln -sf $PWD/.tmux.conf $HOME/.tmux.conf && echo ".tmux.conf linked"
ln -sf $PWD/.config/alacritty $HOME/.config/ && echo ".config/alacritty linked"
ln -sf $PWD/.config/nvim $HOME/.config/ && echo ".config/nvim linked"
ln -sf $PWD/.config/bspwm $HOME/.config/ && echo ".config/bspwm linked"
ln -sf $PWD/.config/sxhkd $HOME/.config/ && echo ".config/sxhkd linked"
ln -sf $PWD/.config/mpd $HOME/.config/ && echo ".config/mpd linked"
ln -sf $PWD/.config/polybar $HOME/.config/ && echo ".config/polybar linked"
ln -sf $PWD/.ncmpcpp $HOME/ && echo ".ncmpcpp linked"
ln -sf $PWD/.config/picom $HOME/.config/ && echo ".config/picom linked"
ln -sf $PWD/.config/streamlink $HOME/.config/ && echo ".config/streamlink linked"
ln -sf $PWD/.config/dunst $HOME/.config/ && echo ".config/dunst linked"
