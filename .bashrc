#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

unalias -a
set -o vi

txtblu='\e[0;34m' # Blue
txtwht='\e[0;37m' # White
txtcyn='\e[0;36m' # Cyan
txtrst='\e[0m'    # Text Reset
PS1='[$?]\u@\h \W\$ '

# fzf fuzzy search keybindings
source /usr/share/fzf/key-bindings.bash
source /usr/share/fzf/completion.bash

# Aliases
#alias vi='nvim'
#alias vim='nvim'
alias vi='vim'
alias ls='ls --color=auto'
alias grep='grep -i --colour=auto'
alias egrep='egrep -i --colour=auto'
alias ssh='TERM=xterm-256color ssh'


# Exports
export EDITOR=vim
export VISUAL=vim
export EDITOR_PREFIX=vim

# Import colorscheme from 'wal' asynchronously
# &   # Run the process in the background.
# ( ) # Hide shell job control messages.
(cat ~/.cache/wal/sequences &)
