"filetype plugin on
filetype plugin indent on

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')

"Plug 'valloric/youcompleteme'
"Plug 'w0rp/ale'
Plug 'sheerun/vim-polyglot'
"Plug 'arcticicestudio/nord-vim'
"Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'dylanaraps/wal.vim'
"Plug 'vim-pandoc/vim-pandoc'
"Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

call plug#end()


syntax on
set background=dark
colorscheme wal
"colorscheme dracula
"colorscheme nord
set number
set relativenumber
set ruler
set smartindent
set smarttab
set autoindent


set cursorline
set nohlsearch
set noincsearch
set linebreak

" X11 clipboard support
set clipboard=unnamedplus

" Nicer set list characters
set listchars=tab:→\ ,eol:↲,nbsp:␣,space:·,trail:·,extends:⟩,precedes:⟨
" On pressing tab, insert 2 spaces
set expandtab
" show existing tab with 2 spaces width
set tabstop=4
set softtabstop=4
" when indenting with '>', use 2 spaces width
set shiftwidth=4

""set ttyfast
set updatetime=300
let mapleader = ","

" unmap arrow keys
noremap <up> :echoerr "use k instead"<CR>
noremap <down> :echoerr "use j instead"<CR>
noremap <left> :echoerr "use h instead"<CR>
noremap <right> :echoerr "use l instead"<CR>
inoremap <up> <NOP>
inoremap <down> <NOP>
inoremap <left> <NOP>
inoremap <right> <NOP>


" easier vim split movement"
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"Fuzzy search on ctrl+p"
nmap <C-P> :FZF<CR>
